//
//  ViewController.swift
//  DeepLink Helper
//
//  Created by Pedro Pereira on 16/08/2019.
//  Copyright © 2019 pmopereira. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private struct pickerData {
        let sourceMarketTitle: String
        let sourceMarketURLScheme: String
    }
    
    private let sourceMarkets = [pickerData(sourceMarketTitle: "BE - TF",
                                            sourceMarketURLScheme: "tuitdatf://"),
                                 pickerData(sourceMarketTitle: "BE - JA",
                                            sourceMarketURLScheme: "tuitdaja://"),
                                 pickerData(sourceMarketTitle: "DE - MT",
                                            sourceMarketURLScheme: "tuidemt://"),
                                 pickerData(sourceMarketTitle: "DE - TC",
                                            sourceMarketURLScheme: "tuidetc://"),
                                 pickerData(sourceMarketTitle: "NL",
                                            sourceMarketURLScheme: "tuitdanl://"),
                                 pickerData(sourceMarketTitle: "NO-FI",
                                            sourceMarketURLScheme: "tuitdafi://"),
                                 pickerData(sourceMarketTitle: "NO-NO",
                                            sourceMarketURLScheme: "tuitdano://"),
                                 pickerData(sourceMarketTitle: "NO-SV",
                                            sourceMarketURLScheme: "tuitdasv://"),
                                 pickerData(sourceMarketTitle: "UK-FC",
                                            sourceMarketURLScheme: "tuitdafc://"),
                                 pickerData(sourceMarketTitle: "UK-TH",
                                            sourceMarketURLScheme: "tuitdamt://")
    ]
    
    var savedDataArray: [URLItem] = []
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var sourceMarketMLabel: UILabel!
    @IBOutlet weak var pickerSuperView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerView.reloadAllComponents()
        pickerSuperView.isHidden = true
        
        setupLabel(market: sourceMarkets[pickerView.selectedRow(inComponent: 0)].sourceMarketTitle)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        sourceMarketMLabel.addGestureRecognizer(tap)
        sourceMarketMLabel.isUserInteractionEnabled = true
        
        savedDataArray = loadData()
        tableView.reloadData()
    }
    
    @IBAction func addAction(_ sender: Any) {
        let alertView = UIAlertController(
            title: "Enter your DeepLink",
            message: "",
            preferredStyle: .alert)
        
        alertView.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Title"
            textField.addTarget(alertView, action: #selector(alertView.textDidChangeAlert), for: .editingChanged)
        }
        
        alertView.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Deeplink"
            textField.addTarget(alertView, action: #selector(alertView.textDidChangeAlert), for: .editingChanged)
        }
        
        let saveAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default, handler: { alert -> Void in
            guard let titleText = alertView.textFields![0].text,
                let urlText = alertView.textFields![1].text else { return }
            
            self.save(item: URLItem(title: titleText,
                                    url: urlText))
            
            self.savedDataArray = self.loadData()
            
            self.tableView.insertRows(at: [IndexPath(row: self.savedDataArray.count - 1, section: 0)], with: .fade)
            
        })
        
        let  cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: nil )
        
        saveAction.isEnabled = false
        
        alertView.addAction(saveAction)
        alertView.addAction(cancelAction)
        
        navigationController?.present(alertView, animated: true, completion: nil)
    }
    
    @IBAction func doneButton(_ sender: Any) {
        pickerSuperView.isHidden = true
        let label = sourceMarkets[pickerView.selectedRow(inComponent: 0)].sourceMarketTitle
        setupLabel(market: label)
        tableView.reloadData()
    }
    
    @IBAction func CancelButton(_ sender: Any) {
        pickerSuperView.isHidden = true
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        pickerSuperView.isHidden = false
    }
    
    func setupLabel(market: String?) {
        if let marketString = market {
            sourceMarketMLabel.text = "Current Source market: " + marketString
        } else {
            sourceMarketMLabel.text = ""
        }
    }
    
    func save(item: URLItem){
        var existingData = loadData()
        existingData.append(item)
        
        do {
            let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: existingData,
                                                                     requiringSecureCoding: false)
            UserDefaults.standard.set(encodedData, forKey: "savedData")
        } catch {
            fatalError("cannot retrive data")
        }
    }
    
    func loadData() -> [URLItem] {
        guard let decoded  = UserDefaults.standard.data(forKey: "savedData") else { return [] }
        
        do {
            guard let decodedData = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? [URLItem] else { return [] }
            return decodedData
        } catch {
            fatalError("cannot retrive data")
        }
    }
    
    func update(index: Int) {
        var existingData = loadData()
        existingData.remove(at: index)
        
        do {
            let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: existingData,
                                                                     requiringSecureCoding: false)
            UserDefaults.standard.set(encodedData, forKey: "savedData")
            savedDataArray = existingData
        } catch {
            fatalError("cannot retrive data")
        }
    }
}
extension ViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return sourceMarkets[row].sourceMarketTitle
    }
}

extension ViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sourceMarkets.count
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let urlItem = savedDataArray[indexPath.row]
        
        let marketDeepLink = sourceMarkets[pickerView.selectedRow(inComponent: 0)].sourceMarketURLScheme
        
        guard let urlToNavigate = URL(string: marketDeepLink + urlItem.url) else { return }
        
        UIApplication.shared.open(urlToNavigate,
                                  options: [:],
                                  completionHandler: nil)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            update(index: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
 
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell!
        
        cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier")
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cellIdentifier")
        }
        
        let urlItem = savedDataArray[indexPath.row]
        
        let marketDeepLink = sourceMarkets[pickerView.selectedRow(inComponent: 0)].sourceMarketURLScheme
        
        cell.textLabel?.text = urlItem.title
        cell.detailTextLabel?.text = marketDeepLink + urlItem.url
        
        return cell
    }
}

extension UIAlertController {
    @objc func textDidChangeAlert() {
        var isValid = true
        
        guard let textFields = textFields else { return }
        
        for textField in textFields {
            if textField.text?.count == 0 {
                isValid = false
            }
        }
        
        if let action = actions.first {
            action.isEnabled = isValid
        }
    }
}
