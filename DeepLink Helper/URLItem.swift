//
//  URLItem.swift
//  DeepLink Helper
//
//  Created by Pedro Pereira on 16/08/2019.
//  Copyright © 2019 pmopereira. All rights reserved.
//

import Foundation

class URLItem: NSObject, NSCoding {
    var title: String
    var url: String
    
    init(title: String, url: String) {
        self.title = title
        self.url = url
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let title = aDecoder.decodeObject(forKey: "title") as! String
        let url = aDecoder.decodeObject(forKey: "url") as! String
        self.init(title: title, url: url)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(title, forKey: "title")
        aCoder.encode(url, forKey: "url")
    }
}
